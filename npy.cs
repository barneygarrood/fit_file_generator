﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.IO.Compression;
using System.Web.Helpers;

namespace FIT_File_Generator
{
  static class npy
  {
    /// <summary>
    /// Loads data from .npy and .json file in the .zip file, returning a dictionary where
    /// channel name is key, and value is the 1D array of datapoints.
    /// </summary>
    /// <param name="zip_file_name"></param>
    /// Name, including path, of .zip file
    /// <param name="file_type"></param>
    ///  Filename root, i.e. "record", "lap", or "session".
    /// <returns></returns>
    public static Dictionary<string, double[]> ReadGridData(string zip_file_name, string file_type)
    {
      Dictionary<string, double[]> ret = new Dictionary<string, double[]>();
      // Read json file:
      dynamic json_data = ReadJson(zip_file_name, file_type);
      if (json_data == null) return ret;
      // Read data file:
      double[,] grid_data = ReadNPY(zip_file_name, file_type);
      // Arrange data:
      int metric_count = grid_data.GetLength(0);
      int point_count = grid_data.GetLength(1);
      for (int i=0;i<metric_count;i++)
      {
        double[] data = new double[point_count];
        for (int j=0;j<point_count;j++)
        {
          data[j] = grid_data[i, j];
        }
        ret.Add(json_data[i],data);
      }
      return ret;
    }


    /// <summary>
    /// reads a file to byte array and parses as json.
    /// </summary>
    /// <param name="filename"></param>
    public static dynamic ReadJson(string zip_file, string file_type)
    {
      //byte[] file_data = ReadBinaryFile(filename);
      byte[] file_data = zip.ReadZipFile(zip_file, file_type,"json");
      // For that you will need to add reference to System.Runtime.Serialization
      string test = System.Text.Encoding.UTF8.GetString(file_data,0,file_data.Length);
      return Json.Decode(test);
    }

    /// <summary>
    /// Read npy file into a double array.  JB says it will always be 64 bit floats.
    /// </summary>
    /// <param name="filename"></param>
    /// <returns></returns>
    public static double[,] ReadNPY(string zip_file, string filename)
    {
      double[,] ret = new double[0, 0];
      try
      {
        Console.WriteLine($"Decoding file {filename} NPY file from {zip_file}");
        // Read file to binary array:
        //byte[] file_data = ReadBinaryFile(filename);
        byte[] file_data = zip.ReadZipFile(zip_file, filename, "npy");

        // Check we have a valid npy file:
        byte[] ident_check = { 0x93, 0x4E, 0x55, 0x4D, 0x50, 0x59 };
        for (int i = 0; i < ident_check.Length; i++)
        {
          if (file_data[i] != ident_check[i])
          {
            Console.WriteLine($"\tFile is not a valid .npy file!");
            return ret;
          }
        }
        Console.WriteLine($"\tConfirm valid npy file.");

        //Check version
        int maj_ver = (int)file_data[6];
        int min_ver = (int)file_data[7];
        Console.WriteLine($"\tNumPy file version {maj_ver}.{min_ver}");

        // Read header.  Firstly get header length:
        int header_length = file_data[8] | (file_data[9] << 8);
        // header is a string:
        string header = Encoding.UTF8.GetString(file_data, 10, header_length);
        // Since we control the data we assume data type is always 8 byte double.
        // So all we need is to extract the size of the array:
        char[] separator = "(".ToCharArray();
        string[] split_header = header.Replace("{", "").Replace("}", "").Split(separator);
        separator = ")".ToCharArray();
        split_header = split_header[1].Split(separator);
        separator = ",".ToCharArray();
        split_header = split_header[0].Split(separator);
        int nrows; int ncols;
        int.TryParse(split_header[0], out nrows);
        int.TryParse(split_header[1], out ncols);
        Console.WriteLine($"\tReading data into {nrows}x{ncols} array.");
        //Put data into array:
        ret = new double[nrows, ncols];
        for (int i = 0; i < nrows; i++)
        {
          for (int j = 0; j < ncols; j++)
          {
            ret[i, j] = BitConverter.ToDouble(file_data, 6 + 2 + 2 + header_length + (i * ncols + j) * 8);
          }
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine($"\tException in NPY file reader.  Message as follows:\n{ex.Message}\n{ex.StackTrace}");
      }
      return ret;
    }

    /// <summary>
    /// Reads a file to a byte array.
    /// </summary>
    /// <param name="filename"></param>
    /// <returns></returns>
    public static byte[] ReadBinaryFile(string filename)
    {
      FileStream fs = new FileStream(filename, FileMode.Open);
      byte[] ret = new byte[fs.Length];
      fs.Read(ret, 0, (int)fs.Length);
      return ret;
    }


  }
}
