﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dynastream.Fit;
using DateTime = Dynastream.Fit.DateTime;
using System.IO;
using System.Diagnostics;

namespace FIT_File_Generator
{
  public static class FIT_Writer
  {
    /// <summary>
    /// Creats dictionary with fitfile name as key, and array of data as value.
    /// </summary>
    /// <param name="raw_data"></param>
    /// <param name="name_translation"></param>
    /// Dictionary of fit name as key, npy name as value.
    /// <param name="fit_config"></param>
    /// <param name="type"></param>
    /// <returns></returns>
    static Dictionary<string, double[]> GetFitData(Dictionary<string, double[]> raw_data,
                                Dictionary<string, string> name_translation,
                                List<string[]> fit_config, string type)
    {
      Dictionary<string, double[]> ret = new Dictionary<string, double[]>();
      for (int i = 0; i < fit_config.Count; i++)
      {
        if ((fit_config[i][0] == type) && (fit_config[i][2] == "1"))
        {
          if (!name_translation.ContainsKey(fit_config[i][1]))
          {
            Console.WriteLine($"\t{type} translation file contains no definition for metric \"{fit_config[i][1]}\".");
          }
          else
          {
            if (raw_data.ContainsKey(name_translation[fit_config[i][1]]))
            {
              if (ret.ContainsKey(fit_config[i][1]))
              {
                Console.WriteLine($"\t{type} config file definition already contains metric \"{fit_config[i][1]}\".");
              }
              else
              {
                ret.Add(fit_config[i][1], raw_data[name_translation[fit_config[i][1]]]);
              }
            }
            else
            {
              Console.WriteLine($"\t{type} npy file does not contain metric \"{name_translation[fit_config[i][1]]}\" requested for fit file metric \"{fit_config[i][1]}\".");
            }
          }
        }
      }
      return ret;
    }



    /// <summary>
    /// Writes data to fit file.
    /// Assumes config file is in root directory called fit_config.csv
    /// data is going to be saved in file with same name as .zip file, but .fit
    /// </summary>
    /// <param name="record_data"></param>
    /// <param name="lap_data"></param>
    /// <param name="section_data"></param>
    /// <param name="fit_config"></param>
    public static void WriteFitFile(
                  Dictionary<string, double[]> record_data,
                  Dictionary<string, double[]> lap_data,
                  Dictionary<string, double[]> section_data,
                  Dictionary<string, string> record_translation,
                  Dictionary<string, string> lap_translation,
                  Dictionary<string, string> section_translation,
                  List<string[]> fit_config,
                  string output_file_path)
    {
      Console.WriteLine("Compiling list of metrics to write to FIT.");
      Dictionary<string, double[]> fit_record_data = GetFitData(record_data, record_translation, fit_config, "record");
      Dictionary<string, double[]> fit_lap_data = GetFitData(lap_data, lap_translation, fit_config, "lap");
      Dictionary<string, double[]> fit_section_data = GetFitData(section_data, section_translation, fit_config, "lap");
      
      //Start time!
      System.DateTime fileStartTime = UnixTimeStampToDateTime(fit_record_data[fit_config[0][1]][0]);
      DateTime timeStamp = new DateTime(fileStartTime);
      Console.WriteLine("Creating FIT messages.  File start time = {0:dd/MM/yy hh:mm}",
                      UnixTimeStampToDateTime(fit_record_data[fit_config[0][1]][0]));
      // get record count:
      int record_count = fit_record_data[fit_record_data.Keys.First()].Length;
      //Calculate elapsed time from grid file:
      float elapsedTime = (float)(fit_record_data[fit_config[0][1]][record_count - 1] - fit_record_data[fit_config[0][1]][0]);

      // Local message type counter.  removes the need to keep putting in descripton messages:
      byte msg_ctr = 0;

      // Generate some FIT messages
      var fileIdMesg = new FileIdMesg(); // Every FIT file MUST contain a 'File ID' message as the first message


      //var records = new List<RecordMesg>();
      List<Mesg> records = new List<Mesg>();
      var laps = new List<LapMesg>();
      //Application id is an arbitrary message to identify source of developer fields.
      char[] velosense = "Velosense".ToCharArray();
      byte[] appID = new byte[16];
      for (int i = 0; i < velosense.Length; i++)
      {
        appID[i] = (byte)velosense[i];
      }
      // Set file ID message - all FIT files must contian this:
      fileIdMesg.SetType(Dynastream.Fit.File.Activity);
      fileIdMesg.SetManufacturer(Dynastream.Fit.Manufacturer.Velosense);                  // Velosense FIT manufacturer ID
      fileIdMesg.SetProduct(null);                      // Eventually need something sensible in here.
      fileIdMesg.SetSerialNumber(1234);                 // and here..
      fileIdMesg.SetTimeCreated(timeStamp);   //Arbitrary date.  Replace with real date when I get it off JB.
      fileIdMesg.LocalNum = msg_ctr;
      msg_ctr++;

      // Activity message:
      var activityMessage = new ActivityMesg();
      activityMessage.SetTimestamp(timeStamp);
      // I am going to ignore local timestamp for now as we don't have that encoded.  Giving the same time as previous means no timezone difference to UTC.
      activityMessage.SetLocalTimestamp(timeStamp.GetTimeStamp());
      activityMessage.SetNumSessions(1);
      activityMessage.SetType(Activity.Manual);
      activityMessage.SetTotalTimerTime(elapsedTime);
      activityMessage.LocalNum = msg_ctr;
      msg_ctr++;

      //Session message:
      var sessionMessage = new SessionMesg();
      sessionMessage.SetTimestamp(timeStamp);
      sessionMessage.SetTotalTimerTime(elapsedTime);
      sessionMessage.SetTotalElapsedTime(elapsedTime);
      sessionMessage.SetSport(Sport.Cycling);
      sessionMessage.LocalNum = msg_ctr;
      msg_ctr++;

      // Associate developer "0" with our app id:
      var developerIdMesg = new DeveloperDataIdMesg();
      for (int i = 0; i < appID.Length; i++)
      {
        developerIdMesg.SetApplicationId(i, appID[i]);
      }
      developerIdMesg.SetDeveloperDataIndex(0);
      developerIdMesg.LocalNum = msg_ctr;
      msg_ctr++;

      //Add developer fields:
      byte dev_field_count = 0;
      List<FieldDescriptionMesg> dev_field_description_msgs = new List<FieldDescriptionMesg>();
      Dictionary<int, DeveloperField> dev_fields = new Dictionary<int, DeveloperField>();
      //List<int> dev_field_indices = new List<int>();
      Dictionary<string, int> dev_field_indices = new Dictionary<string, int>();
      // List unique developer fields.
      List<int> valid_dev_fields = new List<int>();
      List<string> valid_dev_field_names = new List<string>();
      // List dev fields for each type:
      List<int> record_dev_fields = new List<int>();
      List<int> lap_dev_fields = new List<int>();
      List<int> section_dev_fields = new List<int>();
      for (int i = 0; i < fit_config.Count; i++)
      {
        if ((fit_config[i][2] == "1") && (fit_config[i][3] == "1"))
        {
          switch (fit_config[i][0])
          {
            case "record":
              if (fit_record_data.ContainsKey(fit_config[i][1]))
              {
                record_dev_fields.Add(i);
                if (!valid_dev_fields.Contains(i) && !valid_dev_field_names.Contains(fit_config[i][1]))
                {
                  valid_dev_fields.Add(i);
                  valid_dev_field_names.Add(fit_config[i][1]);
                }
              }
              break;
            case "lap":
              if (fit_lap_data.ContainsKey(fit_config[i][1]))
              {
                lap_dev_fields.Add(i);
                if (!valid_dev_fields.Contains(i) && !valid_dev_field_names.Contains(fit_config[i][1]))
                {
                  valid_dev_fields.Add(i);
                  valid_dev_field_names.Add(fit_config[i][1]);
                }
              }
              break;
            case "section":
              if (fit_section_data.ContainsKey(fit_config[i][1]))
              {
                section_dev_fields.Add(i);
                if (!valid_dev_fields.Contains(i) && !valid_dev_field_names.Contains(fit_config[i][1]))
                {
                  valid_dev_fields.Add(i);
                  valid_dev_field_names.Add(fit_config[i][1]);
                }
              }
              break;
          }
        }
      }

      FieldDescriptionMesg fieldDescMesg = new FieldDescriptionMesg();
      for (int i = 0; i < valid_dev_fields.Count; i++)
      {
        // Check channel is both "current" and a developer field.
        if ((fit_config[valid_dev_fields[i]][2] == "1") && (fit_config[valid_dev_fields[i]][3] == "1"))
        {
          Console.WriteLine($"\tCreating developer field {fit_config[valid_dev_fields[i]][1]}");
          dev_field_indices.Add(fit_config[valid_dev_fields[i]][1],i);
          fieldDescMesg = new FieldDescriptionMesg();
          fieldDescMesg.SetDeveloperDataIndex(0);
          fieldDescMesg.SetFieldDefinitionNumber(dev_field_count);
          switch (fit_config[valid_dev_fields[i]][5].ToLower())
          {
            case "enum":
              fieldDescMesg.SetFitBaseTypeId(FitBaseType.Enum);
              break;
            case "sint8":
              fieldDescMesg.SetFitBaseTypeId(FitBaseType.Sint8);
              break;
            case "uint8":
              fieldDescMesg.SetFitBaseTypeId(FitBaseType.Uint8);
              break;
            case "sint16":
              fieldDescMesg.SetFitBaseTypeId(FitBaseType.Sint16);
              break;
            case "uint16":
              fieldDescMesg.SetFitBaseTypeId(FitBaseType.Uint16);
              break;
            case "sint32":
              fieldDescMesg.SetFitBaseTypeId(FitBaseType.Sint32);
              break;
            case "uint32":
            case "date_time (uint32)":
              fieldDescMesg.SetFitBaseTypeId(FitBaseType.Uint32);
              break;
            case "sint64":
              fieldDescMesg.SetFitBaseTypeId(FitBaseType.Sint64);
              break;
            case "uint64":
              fieldDescMesg.SetFitBaseTypeId(FitBaseType.Uint64);
              break;
            case "float32":
              fieldDescMesg.SetFitBaseTypeId(FitBaseType.Float32);
              break;
            case "float64":
              fieldDescMesg.SetFitBaseTypeId(FitBaseType.Float64);
              break;
            default:
              Console.WriteLine("Invalid developer field data type '{0}' for field '{1}' in fit config file.", fit_config[valid_dev_fields[i]][5], 
                fit_config[valid_dev_fields[i]][1]);
              Console.WriteLine("Exiting - no fit file written");
              return;
          }
          //fieldDescMesg.SetFitBaseTypeId(FitBaseType.Float32);
          fieldDescMesg.SetFieldName(0, fit_config[valid_dev_fields[i]][1]);
          fieldDescMesg.SetUnits(0, fit_config[valid_dev_fields[i]][4]);
          // Following allows you to set developer field as an inbuilt FIT field:
          //fieldDescMesg.SetNativeFieldNum(RecordMesg.FieldDefNum.Power);
          fieldDescMesg.LocalNum = msg_ctr;
          dev_field_description_msgs.Add(fieldDescMesg);
          dev_field_count++;
        }
      }
      msg_ctr++;
      byte record_local_num = msg_ctr;
      msg_ctr++;
      byte lap_local_num = msg_ctr;
      Console.WriteLine($"\tWriting {record_count} records.");
      LapMesg lap = new LapMesg();
      // Write data:
      RecordMesg newRecord = new RecordMesg();
      DeveloperField devField;
      for (int i = 0; i < record_count; i++)
      {
        newRecord = new RecordMesg();
        // Timestamp:
        timeStamp = new DateTime(UnixTimeStampToDateTime(fit_record_data["timestamp"][i]));
        newRecord.SetTimestamp(timeStamp);

        //newRecord.SetTimestamp(timeStamp);
        //Standard FIT fields:
        float x = Math.Pow(2, 31) / 180.0d);
        newRecord.SetPositionLat((int)(fit_record_data["position_lat"][i] * Math.Pow(2, 31) / 180.0d));            // convert to semicircles
        newRecord.SetPositionLong((int)(fit_record_data["position_long"][i] * Math.Pow(2, 31) / 180.0d));     // convert to semicircles
        newRecord.SetDistance((float)fit_record_data["distance"][i]);
        newRecord.SetAltitude((float)fit_record_data["altitude"][i]);
        newRecord.SetSpeed((float)fit_record_data["speed"][i]); // Convert from m/sec to kph.
        newRecord.SetPower((ushort)fit_record_data["power"][i]);
        newRecord.SetTemperature((sbyte)fit_record_data["temperature"][i]);
        // Developer fields:
        for (int j = 0; j < record_dev_fields.Count; j++)
        {
          string field_name = fit_config[record_dev_fields[j]][1];
          devField = new DeveloperField(dev_field_description_msgs[dev_field_indices[field_name]], developerIdMesg);
          newRecord.SetDeveloperField(devField);
          devField.SetValue((float)fit_record_data[field_name][i] * double.Parse(fit_config[record_dev_fields[j]][7]));
        }
        newRecord.LocalNum = record_local_num;
        records.Add(newRecord);
      }

      //// Write laps:
      //Console.WriteLine("Writing laps:");
      //Console.WriteLine("\tLap\tStart\tEnd");
      //for (byte i = 0; i < fit_lap_data[fit_lap_data.Keys.First()].Count(); i++)
      //{
      //  Console.WriteLine($"\t{i}\t{UnixTimeStampToDateTime(lap_data[lap_translation["time_start"]][i]).ToString("HH:mm:ss.fff")}\t{UnixTimeStampToDateTime(lap_data[lap_translation["timestamp"]][i]).ToString("HH:mm:ss.fff")}");

      //  lap = new LapMesg();
      //  lap.SetStartTime(new DateTime(UnixTimeStampToDateTime(lap_data[lap_translation["time_start"]][i])));
      //  lap.SetTimestamp(new DateTime(UnixTimeStampToDateTime(lap_data[lap_translation["timestamp"]][i])));
      //  lap.SetTotalElapsedTime((float)lap_data[lap_translation["duration"]][i]);

      //  lap.SetStartPositionLat((int)(lap_data[lap_translation["start_position_lat"]][i] * Math.Pow(2, 31) / 180.0d));
      //  lap.SetStartPositionLong((int)(lap_data[lap_translation["start_position_long"]][i] * Math.Pow(2, 31) / 180.0d));
      //  lap.SetEndPositionLat((int)(lap_data[lap_translation["end_position_lat"]][i] * Math.Pow(2, 31) / 180.0d));
      //  lap.SetEndPositionLong((int)(lap_data[lap_translation["end_position_long"]][i] * Math.Pow(2, 31) / 180.0d));
      //  // Add developer fields:
      //  foreach (string key in fit_lap_data.Keys)
      //  {
      //    if (dev_field_indices.ContainsKey(key))
      //    {
      //      devField = new DeveloperField(dev_field_description_msgs[dev_field_indices[key]], developerIdMesg);
      //      lap.SetDeveloperField(devField);
      //      devField.SetValue(fit_lap_data[key][i]);
      //    }
      //  }
      //  records.Add(lap);
      //}

      // Write sections:
      Console.WriteLine("Writing sections:");
      Console.WriteLine("\tSection\tStart\tEnd");
      for (byte i = 0; i < fit_section_data[fit_section_data.Keys.First()].Count(); i++)
      {
        Console.WriteLine($"\t{i}\t{UnixTimeStampToDateTime(section_data[section_translation["time_start"]][i]).ToString("HH:mm:ss.fff")}\t{UnixTimeStampToDateTime(section_data[section_translation["timestamp"]][i]).ToString("HH:mm:ss.fff")}");
        //Console.WriteLine($"\t{section_data[section_translation["time_start"]][i].ToString("0.000")}\t{section_data[section_translation["timestamp"]][i].ToString("0.000")}");
        lap = new LapMesg();
        lap.SetStartTime(new DateTime(UnixTimeStampToDateTime(section_data[section_translation["time_start"]][i])));
        lap.SetTimestamp(new DateTime(UnixTimeStampToDateTime(section_data[section_translation["timestamp"]][i])));
        lap.SetTotalElapsedTime((float)section_data[section_translation["duration"]][i]);

        lap.SetStartPositionLat((int)(section_data[section_translation["start_position_lat"]][i] * Math.Pow(2, 31) / 180.0d));
        lap.SetStartPositionLong((int)(section_data[section_translation["start_position_long"]][i] * Math.Pow(2, 31) / 180.0d));
        lap.SetEndPositionLat((int)(section_data[section_translation["end_position_lat"]][i] * Math.Pow(2, 31) / 180.0d));
        lap.SetEndPositionLong((int)(section_data[section_translation["end_position_long"]][i] * Math.Pow(2, 31) / 180.0d));
        // Add developer fields:
        foreach (string key in fit_section_data.Keys)
        {
          if (dev_field_indices.ContainsKey(key))
          {
            devField = new DeveloperField(dev_field_description_msgs[dev_field_indices[key]], developerIdMesg);
            lap.SetDeveloperField(devField);
            devField.SetValue(fit_section_data[key][i]);
          }
        }
        records.Add(lap);
      }


      Console.WriteLine($"Writing fit file to {output_file_path}.");
      //Create FIT file:
      Encode fitEncode = new Encode(ProtocolVersion.V20);
      //Create file:
      FileStream fitStream = new FileStream(output_file_path, FileMode.Create, FileAccess.ReadWrite, FileShare.Read);

      //write header:
      fitEncode.Open(fitStream);

      //Encode each message:
      fitEncode.Write(fileIdMesg);
      fitEncode.Write(activityMessage);
      fitEncode.Write(sessionMessage);
      fitEncode.Write(developerIdMesg);
      // Developer fields:
      foreach (FieldDescriptionMesg fdm in dev_field_description_msgs)
      {
        fitEncode.Write(fdm);
      }
      // Records:
      fitEncode.Write(records);

      //Update header datasize and file CRC:
      fitEncode.Close();
      fitStream.Close();

      Console.WriteLine("Written {0} records to fit file: {1}", record_count, output_file_path);
      //}
      //catch (Exception ex)
      //{
      //  StackTrace st = new StackTrace(ex, true);
      //  Console.WriteLine($"Exception in FIT file generation.  Message as follows:\n{ex.Message}\nDetails: {st.ToString()}");
      //}
    }

    public static System.DateTime TimeStampFromFilename(string file_name)
    {
      string name = Path.GetFileNameWithoutExtension(file_name);
      string[] splitName = name.Split("_".ToCharArray());
      int yr; int mo; int day; int hr; int min;
      Int32.TryParse(splitName[1], out yr);
      Int32.TryParse(splitName[2].Substring(0, 2), out mo);
      Int32.TryParse(splitName[2].Substring(2, 2), out day);
      Int32.TryParse(splitName[3].Substring(0, 2), out hr);
      Int32.TryParse(splitName[3].Substring(2, 2), out min);

      System.DateTime ret = new System.DateTime(yr, mo, day, hr, min, 0);



      return ret;

    }

    public static System.DateTime UnixTimeStampToDateTime(double unixTimeStamp)
    {
      // Unix timestamp is seconds past epoch
      System.DateTime dtDateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
      dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
      return dtDateTime;
    }

    public static double DateTimeToFitTime(System.DateTime dateTime) => (UInt32)(dateTime - new System.DateTime(1989, 12, 31, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds;

    public static double UnixTimeToFitTime(double unixTimeStamp) => (UInt32)(unixTimeStamp - (new System.DateTime(1989, 12, 31, 0, 0, 0, System.DateTimeKind.Utc) -
                                                                                                new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc)).TotalSeconds);
  }
}
