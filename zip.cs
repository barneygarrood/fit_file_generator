﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;
using System.IO.Compression;

namespace FIT_File_Generator
{
  public static class zip
  {    
    /// <summary>
    /// Reads data from matching file within the archive at filename.
    /// file_type is the text at the start of the zipped file name, and file_extension the text at the end.
    /// </summary>
    /// <param name="zip_file"></param>
    /// <param name="file_name"></param>
    /// <param name="file_extension"></param>
    /// <returns></returns>
    public static byte[] ReadZipFile(string zip_file, string file_name, string file_extension)
    {
      byte[] ret = new byte[0];
      try
      {
        using (ZipArchive zip = ZipFile.OpenRead(zip_file))
        {
          foreach (ZipArchiveEntry entry in zip.Entries)
          {
            if (Regex.Match(entry.Name, String.Format("^{0}.*{1}$", file_name, file_extension)).Success)
            {
              Stream fs = entry.Open();
              ret = new byte[fs.Length];
              fs.Read(ret, 0, (int)fs.Length);
              return ret;
            }
          }
        }
      }
      catch
      {
        Console.WriteLine($"There was a problem opening file: {zip_file}");
      }
      return ret;
    }
  }
}
