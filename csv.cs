﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.IO.Compression;

namespace FIT_File_Generator
{
  static class csv
  {
    public static List<string[]> ReadCSV(string zip_file_path, string csv_file, bool discard_header = true)
    {
      List<string[]> ret = new List<string[]>();
      if (!System.IO.File.Exists(zip_file_path))
      {
        Console.WriteLine($"Config file not found: {zip_file_path}");
        return ret;
      }
      byte[] csv_data = zip.ReadZipFile(zip_file_path, csv_file, "csv");
      //Read csv and parse into fit_config object:
      using (var reader = new StreamReader(new MemoryStream(csv_data)))
      {
        string line = "";
        if (discard_header) line = reader.ReadLine();   //Discard header line.
        while (!reader.EndOfStream)
        {
          line = reader.ReadLine();
          if (line.Substring(0, 2) != "//")
          {
            string[] values = line.Split(",".ToCharArray());
            // trim entries.
            for (int i = 0; i < values.Length; i++)
            {
              values[i] = values[i].Trim();
            }
            ret.Add(values);
          }
        }
        reader.Close();
      }
      return ret;
    }

    /// <summary>
    /// Takes csv file and puts first two columns into a dictionary.
    /// First column is value, second column is key.
    /// </summary>
    /// <param name="zip_file_path"></param>
    /// <param name="csv_file"></param>
    /// <returns></returns>
    public static Dictionary<string,string> CSVToDictionary(string zip_file_path, string csv_file, bool discard_header = true)
    {
      List<string[]> csvfile = ReadCSV(zip_file_path, csv_file, discard_header);
      Dictionary<string, string> ret = new Dictionary<string, string>();
      foreach (string[] x in csvfile)
      {
        if (x.Length>1)
        {
          if (ret.ContainsKey(x[0]))
          {
            Console.WriteLine($"\t{csv_file} translation file contains duplicate channel {x[0]}");
          }
          else if (x[1] == "X")
          {
            Console.WriteLine($"\t{csv_file} translation file has no definition for channel {x[0]}");
          }
          else
          {
            ret.Add(x[0], x[1]);
          }
        }
      }
      return ret;
    }
  }
}
