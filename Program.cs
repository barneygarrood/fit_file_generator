﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace FIT_File_Generator
{
  class Program
  {
    static void Main(string[] args)
    {
      Stopwatch sw = new Stopwatch();
      sw.Start();
      Console.WindowWidth = 200;
      //for testing, if no argument given, use a fixed address.  This won't work on others computers.
      if (args.Length==0)
      {
        args = new string[1];
#if DEBUG
        args[0] = @"C:\Dropbox\Velosense\Testing\Roadtests\Raw_Data\Grid_Files\prefit_2019_1129_145418.zip";
#else
        Console.WriteLine("Usage: NPY_FIT_Converter <file_name.zip>");
        goto Finish;
#endif // DEBUG
      }
      // Check file exists:
      if (!File.Exists(args[0]))
      {
        Console.WriteLine($"File not found: {args[0]}");
        goto Finish;
      }
      Dictionary<string, double[]> record_data = npy.ReadGridData(args[0], "record");
      Dictionary<string, double[]> lap_data = npy.ReadGridData(args[0], "lap");
      Dictionary<string, double[]> section_data = npy.ReadGridData(args[0], "section");
      if (record_data.Count > 0)
      {
        Console.WriteLine($"Found {record_data.Keys.Count} fields with {record_data[record_data.Keys.First()].Length} records in record data:");
        foreach (string key in record_data.Keys)
        {
          Console.WriteLine("\t{0}", key);
        }
        Console.WriteLine($"Found {lap_data.Keys.Count} fields with {lap_data[lap_data.Keys.First()].Length} records in lap data:");
        foreach (string key in lap_data.Keys)
        {
          Console.WriteLine("\t{0}", key);
        }
        Console.WriteLine($"Found {section_data.Keys.Count} fields with {section_data[section_data.Keys.First()].Length} records in section data:");
        foreach (string key in section_data.Keys)
        {
          Console.WriteLine("\t{0}", key);
        }
        Console.WriteLine("Getting translation dictionaries from csv files.");
        List<string[]> fit_config = csv.ReadCSV(args[0], "fit_config");
        Dictionary<string, string> record_translation = csv.CSVToDictionary(args[0], "record", false);
        Dictionary<string, string> lap_translation = csv.CSVToDictionary(args[0], "lap", false);
        Dictionary<string, string> section_translation = csv.CSVToDictionary(args[0], "section", false);

        // Write fit file:
        string output_file_path = args[0].Replace(".zip", ".fit");
        output_file_path = output_file_path.Replace("prefit_", "");
        FIT_Writer.WriteFitFile(record_data,lap_data,section_data, record_translation, lap_translation,
          section_translation, fit_config, output_file_path);
      }
    Finish:
      Console.WriteLine("Time taken = {0}ms", sw.ElapsedMilliseconds);
      sw.Stop();
      Console.ReadKey();
    }
  }
}

